import java.util.Arrays;
import java.util.concurrent.Semaphore;
public class DatingGame {
	static int num_contestant, num_rounds,num_date, group_size;;
	static Contestant[] contestant;
	static Date[] date;
	static SmartPants smartPants;
	static boolean[] isContestantFinished;
	
//	boolean contestantGone[];
//	boolean smartPantSignal[];
//	boolean contestantSignal[];
//	boolean readyToEnterClub;
//	boolean datesAvailable[];
//	boolean smartPantsAwake;
//	boolean contestantBackToTable[];
//	
//	int returnedToTableContestants;
//	int whoAskTheDate[];
//	int watingQueueForSmartPants[];
//	int datesArrivalQueue[];
//	int decisionByDates[];
//	int dates_count;
//	int num_cont_entered_club;
//	Semaphore sempForDate = new Semaphore(1);
//	Semaphore sempForAsk = new Semaphore(1);
//	Semaphore sempForQueue = new Semaphore(1);
//	Semaphore sempForMeetingSmartPants = new Semaphore(1);
//	Semaphore sempForDone = new Semaphore(1);
//	Semaphore sempForEntering = new Semaphore(1);
//	Semaphore sempForAppraod = new Semaphore(1);
//	Semaphore sempGroup = new Semaphore(0);
//	Semaphore sempContestantFinished = new Semaphore(1);
//	Semaphore sempEnd = new Semaphore(0);
//	Semaphore sempAllContestantOver = new Semaphore(0);
//	Semaphore sempComtestantComing = new Semaphore(0);
	
	int totalApproach;
	//boolean show_ends;
	boolean smartPantGoHome;
	boolean getPatOnBySmartPants[];
	
	
	protected Semaphore waitForSmartPants;
	protected Semaphore Contestant_arrived;
	protected static Semaphore mutex;
	protected Semaphore letContestantMeetSP;
//	protected Semaphore dateAvailable;
	
	protected static int contestantArrivedQ[];
	protected static int num_of_contestant_initiated;
	static int meetWithSmartPants;
	static int countOfContest;
	int contestant_done;
//	boolean datesArrived[];
	Semaphore datefree[];
	//constructor
	DatingGame(int num_contestant, int num_rounds,int num_date, int group_size)
	{
		DatingGame.num_contestant = num_contestant;
		DatingGame.num_rounds = num_rounds;
		DatingGame.num_date = num_date;
		DatingGame.group_size = group_size;
		countOfContest = 0;
		meetWithSmartPants = 0;
		contestant_done = 0;
		contestant = new Contestant[num_contestant];
		date = new Date[num_date];
		contestantArrivedQ = new int[num_contestant];
//		datesArrived = new boolean[num_date];
		datefree = new Semaphore[num_date];
		num_of_contestant_initiated = 0;
		waitForSmartPants = new Semaphore(0);
		Contestant_arrived  = new Semaphore(0);
		letContestantMeetSP = new Semaphore(0);
//		dateAvailable = new Semaphore(0);
		for(int i = 0; i < num_date; i++)
		{
//			datesArrived[i] = false;
			datefree[i] = new Semaphore(0);
		}
		mutex = new Semaphore(1);
	}
	public static void setContestantArrived(int contestantID) throws InterruptedException
	{
		mutex.acquire();
		contestantArrivedQ[countOfContest++] = contestantID;
		mutex.release();
		System.out.println(Arrays.toString(contestantArrivedQ));
	}
	public static void updateInitiatedContestant() 
	{
		num_of_contestant_initiated++;
	}
	public static void updateDoneMeetingSP() throws InterruptedException
	{
		mutex.acquire();
		meetWithSmartPants++;
		mutex.release();
	}
	public static void main(String[] args)
	{
		if(args.length == 4)
		{
			try
			{
				num_contestant = Integer.parseInt(args[0]);
				num_date = Integer.parseInt(args[1]);
				num_rounds = Integer.parseInt(args[2]);
				group_size = Integer.parseInt(args[3]);
				
			}
			catch(Exception e)
			{
				System.out.println("Usage:  number_of_contestant number_of_date number_of_rounds");
			}
			DatingGame game = new DatingGame(num_contestant,num_rounds ,num_date,group_size);
			
			game.initialize(game);
		}
		else
		{
			System.out.println("Invalid argument parameters! ");
			System.exit(0);
		}
	}
	//to initialise threads
	public void initialize(DatingGame game)
	{
		smartPants = new SmartPants("SmartPants", game);
		for(int i = 0; i < num_contestant; i++)
		{
			contestant[i] = new Contestant("Contestant:" + i, game);
		}
		
		for(int i = 0; i < DatingGame.num_date; i++)
		{
			game.date[i] = new Date("Date:" + i, game);
		}
	}

}
