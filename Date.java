import java.util.Arrays;
import java.util.Random;

public class Date implements Runnable{
	private String name;
	private DatingGame game;
	private static long time; 
	private boolean askedByContest[];
	Thread t;
	public Date(String name, DatingGame game)
	{
		this.name = name;
		this.game = game;
		t = new Thread(this,name);
		t.start();
		askedByContest = new boolean[DatingGame.num_contestant];
		Arrays.fill(askedByContest, false);
		time = System.currentTimeMillis(); 
	}

	
	/*
	 * dates are in the club and ready to be asked
	 */
	public void dateArrived()
	{
		sleep(randomTime(3000));
		msg("arrived at the club");
	}
	
	//signal contestants dates are available to be asked
	public void signalAvalibleForApproach()
	{
//		game.datesArrived[getID()] = true;
//		game.dateAvailable.release();
		game.datefree[getID()].release();
	}
	public void sleep(long time)
	{
		try {
			Thread.sleep(time);
			}
		catch (InterruptedException e) {
			e.printStackTrace();}
	}
	public void msg(String m)
	{
		System.out.println("["+(System.currentTimeMillis()-time)+"] "+getName()+": "+m);
	}
	public String getName()
	{
		return this.name;
	}
	public long randomTime(long time)
	{
		Random r =new Random();
		long randTime=(long) (Math.abs((r.nextInt() % time)) + 1000);
		return randTime;
	}
	public int getID()
	{
		//could optimise how to read integers in contestants name
		String ID = this.name.charAt(5)+"";
		return Integer.parseInt(ID);
	}
	
	public void run()
	{
		while(game.meetWithSmartPants!= game.num_contestant)
		{
			sleep(3000);
		}
		
		dateArrived();
		
		signalAvalibleForApproach();
			
	
	}
	
	public void chattingwithContestant ()  throws InterruptedException
	{
		Thread.sleep(randomTime(4000));
	}
}
