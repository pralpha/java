import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

public class Contestant implements Runnable {
	private String name;
	private DatingGame game;
	private static long time;
	private int rounds;
	private boolean[] dates_asked;
	private int[] acceptedOrRejected;
	// private ArrayList<Integer> group;
	Thread t;

	public Contestant(String name, DatingGame game) {
		this.name = name;
		this.game = game;
		t = new Thread(this, name);
		t.start();
		time = System.currentTimeMillis();
	}

	/*
	 * contestant arrived
	 */
	public void arrived() throws InterruptedException 
	{
		Thread.sleep(randomTime(4000));
		msg("arrived");
		//signal to smartpants contestant is available to meet
		
		DatingGame.setContestantArrived(getID());
		game.Contestant_arrived.release();
	}
	
	public void meetingSmartPants() throws InterruptedException
	{
		msg("Waiting for smartPants");
		game.letContestantMeetSP.acquire();//wait for smartpants' signal to meet
		msg("meeting smartPants");
		DatingGame.updateDoneMeetingSP();
	}
	
	
	public void run() 
	{
		try 
		{
			arrived();
			meetingSmartPants();
			
			enteringTheClub();
			
			waitingForDates();
		
		} 
		catch (InterruptedException e){e.printStackTrace();}
	}
	
	
	/** not done yet, has BUG!!!
	 */
	public void waitingForDates() throws InterruptedException
	{
		while(rounds < DatingGame.num_rounds )
		{
			for(int i = 0; i < DatingGame.num_date;i++)
			{
//				if(game.datesArrived[i])
//				{
					//can't use--> game.dateAvailable.acquire() here
//					msg("Wating to chat with " + i);
					if(game.datefree[i].tryAcquire())
					{
						chatingWithDate(i);
						game.datefree[i].release();
						rounds++;
						if(rounds >= DatingGame.num_rounds)
						{
							break;
						}
					}
//				}
			}
		}
	}
		
	public void chatingWithDate(int DateID) throws InterruptedException
	{
		msg("---chatting with Date: "+DateID);
		game.date[DateID].chattingwithContestant();
		msg("***Done chatting with "+DateID);

	}
	public void enteringTheClub()
	{
		//after everyone meeting with smartpants, enter the club  together
		while(DatingGame.meetWithSmartPants < DatingGame.num_contestant)
		{
			try 
			{
				Thread.sleep(500);
			} 
			catch (InterruptedException e) {e.printStackTrace();}
		}
		msg("enter the club and ready to meet a date");
	}
	public void msg(String m) {
		System.out.println("[" + (System.currentTimeMillis() - time) + "] "
				+ getName() + ": " + m);
	}

	public String getName() {
		return this.name;
	}

	// generate random value for sleep time
	public long randomTime(long time) {
		Random r = new Random();
		long randTime = (long) (Math.abs((r.nextInt() % time)) + 1000);
		return randTime;
	}
	public int getID() 
	{
		String ID = this.name.charAt(11) + "";
		return Integer.parseInt(ID);
	}
	
}