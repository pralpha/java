import java.util.Random;
import java.util.concurrent.Semaphore;

public class SmartPants implements Runnable{
	private String name;
	private static long time;
	private DatingGame game;
	private int contestantID = 0;
	Semaphore sempFinished = new Semaphore(0);
	Thread spT;
	public SmartPants(String name, DatingGame game)
	{
		this.name = name;
		this.game = game;
		spT = new Thread(this,name);
		spT.start();
		contestantID = 0;
		time = System.currentTimeMillis(); 
	}
	
	public void smartPantsArrival()
	{
		System.out.println("======================<<<Dating Game>>>=====================");
		msg("Welcome to tonight's Dating Game Show!");
	}
	
	
	public void run()
	{
		try 
		{
			smartPantsArrival();
			
			meetContestant();
			
		} catch (InterruptedException e) {e.printStackTrace();}
		
		waitForContestantToBeDone();
	}
	
	public void meetContestant() throws InterruptedException
	{
		while(DatingGame.num_of_contestant_initiated < DatingGame.num_contestant)
		{
			game.Contestant_arrived.acquire();//meet contestant when they arrive
			msg("Welcome to the game Contestant: " + game.contestantArrivedQ[contestantID++]);
			DatingGame.updateInitiatedContestant();
			game.letContestantMeetSP.release();//allow contestant to meet smartpants one at a time
		}
	}
	
	public void waitForContestantToBeDone()
	{
		while(game.contestant_done != game.num_contestant)
		{
			sleep(3000);
		}
	}

	public void msg(String m)
	{
		System.out.println("["+(System.currentTimeMillis()-time)+"] "+getName()+": "+m);
	}
	
	public String getName()
	{
		return this.name;
	}
	
	
	//sleep for random time
	public void sleep(long time)
	{
		try {Thread.sleep(time);}
		catch (InterruptedException e) {msg("sleep interrupted");}
	}
	
	//generate random sleep time
	public long randomTime(long time)
	{
		Random r =new Random();
		long randTime=(long) (Math.abs((r.nextInt() % time)) + 1000);
		return randTime;
	}
	
	
}
